<br>
<br>
<h3>SDL</h3>
<ul>
<li><a href="https://ru.wikipedia.org/wiki/Simple_DirectMedia_Layer">Краткое описание</a> на Википедии
<li><a href="http://libsdl.org/">Официальный сайт</a>
<li><a href="https://github.com/libsdl-org/SDL">Исходный код</a> на github-е.
</ul>

Для работы с png-изображениями и звуком использую библиотеки <a href="https://github.com/libsdl-org/SDL_image">SDL_image</a> и <a href="https://github.com/libsdl-org/SDL_mixer">SDL_mixer</a>.
<br>

<h3>Box2D</h3>
<ul>
<li><a href="https://ru.wikipedia.org/wiki/Box2D">Краткое описание</a> на Википедии
<li><a href="https://box2d.org/">Официальный сайт</a>
<li><a href="https://github.com/erincatto/box2d">Исходный код</a> на github-е.
</ul>

<h3>Дополнительные инструменты</h3>
<ul>
<br>
<li>Редактор уровней <a href="https://www.mapeditor.org/">Tiled Map Editor</a>.
Карта уровня состоит из нескольких слоев, background-слой разбит на квадратные тайлы размером 64x64 пикселей, проект уровня выглядит вот так, пунктирными линиями показана разметка с шагом 64 пикселя:
<br>
<br>
<img src="https://habrastorage.org/webt/dc/xq/ed/dcxqedlx9aymcuriyxnuyt0texc.png" />
<br>
Редактор позволяет выгружать уровни в различные форматы, я выгружаю в XML + использую свою кастомную XSD для последующей валидации своих ошибок.
<br>
<br>
<li>Онлайн-утилита для генерации атласов <a href="https://www.leshylabs.com/apps/sstool/">Leshy SpriteSheet Tool</a>
<li><a href="https://github.com/libgdx/libgdx/wiki/Distance-field-fonts">Hiero</a> для генерации <a href="https://habr.com/ru/post/282191/">SDF-шрифтов</a> и иконок
</ul>

<h3>Графика и анимации</h3>
Все графические элементы и анимации были куплены на сайте <a href="https://pixelfrog-store.itch.io/pirate-bomb">itch.io</a>.
<br>
<br>
<h2>Описание архитектуры</h2>
<br>
Далее я привожу несколько диаграмм, описывающих те или иные аспекты архитектуры. Диаграммы несколько упрощены, чтобы избежать нагромождения, в них могут быть не отражены некоторые малозначительные детали. Моя цель - не задокументировать точную архитектуру, а описать идею.
<br>
Все диаграммы были написаны на сайте <a href="https://liveuml.com/">liveuml.com</a>.
<br>
Полезная ссылка-helper:<a href="https://plantuml.com/class-diagram">https://plantuml.com/class-diagram</a>.
<br>
<br>
В C++ нет такого объекта, как интерфейс, его роль выполняет абстрактный класс, но тем не менее я помечаю такой объект на диаграмме символом "I". Если какой-то класс наследует интерфейс/родительский класс, я привожу список методов в интерфейсе/родительском классе и не дублирую их в описании дочернего класса. Все классы, не являющиеся интерфейсами (абстрактными классами), я помечаю символом "C". Для обозначения отношений между объектами я использую два типа связей, покажу их на диаграмме ниже:

```plantuml
class Map
class Game
Game --> Map
note on link : Объект класса Game использует\nобъект класса Map.

interface Controller
Controller ^-- TSController
note on link : Класс TSController реализует/наследует\nкласс Controller. Controller - родительский\nкласс по отношению к TSController.
```

<h3>Диаграмма компонентов</h3>
Я декомпоизировал свое приложение на 5 основных компонента (для простоты можно считать, что каждый компонент определен в одноименном неймспейсе):
<ul>
<li>Main - точка входа в приложение;</li>
<li>2DEngines - классы, управляющие расчетом физики твердых тел;</li>
<li>Controllers - классы для управления игровым процессом, в частности, главным героем;</li>
<li>Renderers - отрисовка;</li>
<li>PBGame (Pirate Bomb Game) - компонент, в котором сосредоточена вся игровая логика, за исключением всего того, что перечислено в предыдущих пунктах.</li>
</ul>

Зависимости между компонентами построены таким образом, чтобы защитить основной компонент PBGame от изменений в других компонентах: PBGame не зависит ни от одного из компонентов. Наоборот, все остальные компоненты зависят от него, и стрелки, пересекающие границы компонента PBGame, пересекают его границы в направлении снаружи внутрь. Верхоуровневая диаграмма компонентов выглядит так:

```plantuml
package "Main" <<Frame>> {
class Main
}

package "PBGame" <<Frame>> {
  class Game {
  }

  class Map

  interface       Controller
  interface       ObjectsVisitor
  interface       Engine2D
}

Game --> Map
Game --> Controller
Game --> Engine2D
ObjectsVisitor ^-- Engine2D

package "2DEngines" <<Frame>> {
Engine2D ^-- Box2D_Engine
Engine2D ^-- Chipmunk2D_Engine
}

package "Controllers" <<Frame>> {
  Controller ^-- PCController
  Controller ^-- TSController
  Controller ^-- HIDController
}

package "Renderers" <<Frame>> {
  class SDLRenderer
  ObjectsVisitor ^-- SDLRenderer
}

Main --> Game
Main --> SDLRenderer

note bottom of PCController : Mouse and\nkeyboard
note bottom of TSController : TouchScreen
note bottom of HIDController : Any joystick\ndevice

note bottom of Box2D_Engine : Abstraction over\nBox2D
note bottom of Chipmunk2D_Engine : Abstraction over\nChipmunk2D
```

Далее детальнее рассмотрим каждый из этих компонентов.

<h3>Интеграция с Box2D</h3>
<br>
Мы пишем 2D-платформер с различными объектами мира: одушевленными (персонажи), так и не одушевленными (столы, стулья, бочки, бутылки и.т.д). Все эти объекты имеют свои физические свойства: массу, форму, характеристики трения, упругости и т.п. Для моделирования поведения наших объектов в пространстве будем использовать open-source движок Box2D.
<br>
<br>
Рассмотрим принцип на самом обобщенном примере - персонаже. Для того, чтобы смоделировать его поведение нам необходимо выделить две области памяти:
<ul>
<li>объект класса b2Body - структуры, определенной в неймспейсе Box2D, которая служит интерфейсом для установки и получения физических характеристик объекта. Самые частые операции обращения к нему - это получение его текущего местоположения, угла поворота, и чуть реже вектора скорости. За выделение памяти под этот объект отвечает Box2D, наша задача - задать правильные свойства тела: размер, форму, параметры трения и упругости</li>
<li>Character - область памяти, находящася в области ответственности нашего приложения, в которой хранится вся дополнительная информация, соответствующая игровой логике персонажа (текущее состояние: стоит/бежит/прыгает/падает, направление вгляда, количество жизней и т.д.)</li>
</ul>
После создания этих объектов определим двусторонную связь между ними посредством указателей (для управления указателем на персонажа используются функции-члены SetUserData и GetUserData. Намеренно не привожу их полное описание, дабы не загромождать диаграмму).
<br>

```plantuml
package PBGame <<Frame>> {
  class Character {
  Status      status;
  ...
  void*       b2body;
  ...
  }
}

package Box2D <<Frame>> {
    class b2Body {
    {method}...
    const b2Vec2&   GetPosition() const;
    float32               GetAngle() const;
    const b2Vec2& GetLinearVelocity() const;
    void ApplyForceToCenter(...);
    void ApplyLinearImpulse(...);
    void SetFixedRotation(bool flag);
    void SetUserData(void* data);
    void* GetUserData() const;
    {method}...
    }
}

b2Body --> Character
Character --> b2Body
```
Такая связь необходима, чтобы можно было инициировать управляющее воздействие с любой из сторон. Например, если в области ответственности класса Character мы приняли решении о движении, то мы должны сообщить об этом движку Box2D через обращение к объекту b2Body (SetLinearVelocity). Или наоборот, в момент, когда персонаж падает на землю, мы, отловив момент соприкосновения персонажа с телом земли, должны послать сигнал от b2Body к Character, чтобы его текущий статус (и связанные с ним анимации) изменился на "приземление".

В реальности же у нас не будет прямого обращения из кода компонента игры к Box2D API, мы скроем его за абстракцией Box2D_Engine, продемонстрирую это несколько позже.

<h3>Контроллер управления главным персонажем</h3>

Тут все просто, имеем три реализации одного интерфейса, и подкладываем ту и или иную в зависимости от используемого оборудования. HIDController в настоящее время пока не реализован.
Здесь стрелочка от Game-компонента к интерфейсу Controller означает, что игра во время обновления своего состояния опрашивает текущее состояние контроллера.

```plantuml
package PBGame <<Frame>> {
    class Game
    interface       Controller {
        bool isLeftButtonPressed();
        bool isRightButtonPressed();
        bool isBombButtonPressed();
        bool isJumpButtonPressed();
    }
}

package "Controllers" <<Frame>> {
Controller ^-- PCController
Controller ^-- TSController
Controller ^-- HIDController
}
Game       --> Controller

note bottom of PCController : Mouse and keyboard
note bottom of TSController : TouchScreen
note bottom of HIDController : Any joystick device
```

<h3>Базовые примитивные типы</h3>
Приведенные ниже типы просты, познакомимся с ними здесь, так как они используются в диаграмме следующего подраздела.

```plantuml
package "PODs and Enums" <<Frame>> {
  enum HDirection {
  Left
  Right
  None
  }
  enum VDirection {
  Down
  Up
  None
  }
  enum HFlip {
  Yes
  No
  }
  class FVector {
  float x;
  float y;
  FVector();
  FVector(float X, float Y);
  }
}

note bottom of HDirection : Направление\nдвижения\nпо горизонтали
note bottom of VDirection : Направление\nдвижения\nпо вертикали
note bottom of HFlip : Принак отражения\nпо горизонтали\n(для определения\nнаправления взгляда\nперсонажа)
note bottom of FVector : Вещественный\nвектор для\nхранения скорости\nи положения
```

<h3>Иерархия объектов</h3>
Все объекты унаследованы от базового класса Body.

```plantuml
skinparam defaultFontName Consolas

interface Body {
    const HFlip    initHFlip;
    const double initAngle;
    const FVector initPos;

    Engine2D*     engine;
    void*            b2body;
    HFlip            hFlip;


    FVector               getInitPos() const;
    double                getInitAngle() const;
    HFlip                 getInitHorizontalFlip() const;

    FVector               getPosition() const;
    Direction             getDirection() const;
    double                getAngle() const;
    HFlip                 getFlip() const;
    Uint32                getAnimationIndex();

    {method}// virtual
    void                  handleStatus();
    void                  visit(ObjectVisitor& Visitor);
    void                  setPlatform(PlatformIdType PlatformId);
    void                  handleCollideWithGuy();
    void                  handleBombDamageStatus();
    void                  handleAttackDamageStatus();
}

class    Bomb {
Status : {
-- On,
-- Off,
-- Explosion,
-- Inactive
};
Status getStatus();
}

interface Character {
bool isAlive() const;
}

class Door {
Status : {
-- Closed,
-- Opened,
-- Opening,
-- Closing
};
Status getStatus();
}

class GoldBar {
Status : {
-- Active,
-- Inactive
};
Status getStatus();
}

class Map {
Guy guy;
std::vector<Character> Characters;
std::vector<Bomb> Bombs;
std::vector<Door> Doors;
std::vector<GoldBar> GoldBars;
std::vector<Body> Objects;
}

Map --> Bomb
Map --> Door
Map --> GoldBar
Map --> Character
Map --> Body

Body ^-- OtherObjects

Body ^-- Bomb
Body ^-- GoldBar
Body ^-- Door

Body ^-- Character

Character ^-- Guy
Character ^-- Captain
Character ^-- BaldPirate
Character ^-- BigGuy
Character ^-- Cucumber
Character ^-- Whale

Guy        : <img:https://habrastorage.org/webt/ta/e6/ud/tae6udlqoyqi4lzj5zlqkw4zi2a.png>
BaldPirate : <img:https://hsto.org/webt/oc/jn/ki/ocjnkini_uzyq6u4lak4qajfkuw.png>
Cucumber   : <img:https://habrastorage.org/webt/pp/tf/hv/pptfhvpvqrsdpxf2klr2d8nmcgk.png>
BigGuy     : <img:https://habrastorage.org/webt/or/aw/nt/orawntjxrji6e6mkq_ffwmixpam.png>
Captain    : <img:https://habrastorage.org/webt/fk/zv/jt/fkzvjthd0mztyqmojmgouu2fuj4.png>
Whale      : <img:https://habrastorage.org/webt/hd/j5/jd/hdj5jdbb_rggk1u7wqfrk0obzfu.png>
```

<h3>Паттерн "Посетитель"</h3>
Возникает необходимость обхода вышеприведенной иерархии и обработки ее элементов при решении следующих задач:
<ul>
<li>регистрация объектов в мире Box2D/Chipmunk2D (интерфейс Engine2D);</li>
<li>отрисовка объектов (объект класса SDLRenderer).</li>
</ul>

Если вы еще не знакомы с этим паттерном проектирования, рекомендую посмотреть примеры реализации его  <a href="https://ru.wikipedia.org/wiki/%D0%9F%D0%BE%D1%81%D0%B5%D1%82%D0%B8%D1%82%D0%B5%D0%BB%D1%8C_(%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)">на разных языках</a>.

Показать, как будет выглядеть метод визитера внутри любого из классов

```plantuml
package PBGame <<Frame>> {

interface       ObjectsVisitor {
    {method}void beforeVisit();
    {method}void afterVisit();

    {method}// Персонажи
    {method}void visitGuy(Guy* Guy);
    {method}void visitCaptain(Captain* Captain);
    {method}void visitBaldPirate(BaldPirate* BaldPirate);
    {method}void visitBigGuy(BigGuy* BigGuy);
    {method}void visitCucumber(Cucumber* Cucumber);
    {method}void visitWhale(Whale* Whale);

    {method}// Динамические (движимые) объекты
    {method}void visitBarrel(Barrel* Barrel);
    {method}void visitChair(Chair* Chair);
    {method}void visitTable(Table* Table);
    {method}void visitSkull(Skull* Skull);
    {method}void visitRedBottle(RedBottle* RedBottle);
    {method}void visitGreenBottle(GreenBottle* GreenBottle);
    {method}void visitBlueBottle(BlueBottle* BlueBottle);
    {method}void visitGoldBar(GoldBar* GoldBar);
    {method}void visitBomb(Bomb* Bomb);

    // Статические (недвижимные) объекты
    {method}void visitDoor(Door* Door);
    {method}void visitCandle(Candle* Candle);
    {method}void visitSmallChain(SmallChain* SmallChain);
    {method}void visitBigChain(BigChain* BigChain);
    {method}void visitWindow(Window* Window);
}

interface       Engine2D {
    FVector   getPosition(const void* Body) const;
    Direction getDirection(const void* Body) const;
    double    getAngle(const void* Body) const;
    void      createWorld(const Map* map);
    void      removeBody(void* Body);
    void      update(TimeType msDiff);
    void      explodeBomb(void* Body);

    void      startAttackBigGuy(void* BigGuy, HDirection Dir);
    void      startAttackPirate(void* Pirate, HDirection Dir);
    void      startAttackCaptain(void* Captain, HDirection Dir);
    void      startAttackWhale(void* Whale, HDirection Dir);
    void      startAttackCucumber(void* Cucumber, HDirection Dir);

    void      hitAttackBigGuy(void* Guy, HDirection Dir);
    void      hitAttackPirate(void* Guy, HDirection Dir);
    void      hitAttackCaptain(void* Guy, HDirection Dir);
    void      hitAttackWhale(void* Guy, HDirection Dir);
    void      hitAttackCucumber(void* Guy, HDirection Dir);

    void      jumpGuy(void* Guy);
    void      jumpCaptain(void* Guy);
    void      jumpBigGuy(void* Guy);
    void      jumpPirate(void* Guy);
    void      jumpWhale(void* Guy);
    void      jumpCucumber(void* Guy);

    void      moveGuy(void* Body, HDirection Dir);
    void      moveCaptain(void* Body, HDirection Dir);
    void      moveBigGuy(void* Body, HDirection Dir);
    void      movePirate(void* Body, HDirection Dir);
    void      moveWhale(void* Body, HDirection Dir);
    void      moveCucumber(void* Body, HDirection Dir);

    void      stop(void* Body);
}

class Game {
    Engine2D* engine2D;
    {method}void Update(TimeType msDiff);
}

ObjectsVisitor ^-- Engine2D
}

package "2DEngines" <<Frame>> {
Engine2D ^-- Box2D_Engine
Engine2D ^-- Chipmunk2D_Engine
}

package "Renderers" <<Frame>> {
ObjectsVisitor ^-- SDLRenderer
}

Game --> Engine2D

note bottom of Box2D_Engine : Abstraction over\nBox2D
note bottom of Chipmunk2D_Engine : Abstraction over\nChipmunk2D
```
Вот так, например, будет выглядеть вызов метода посетителя внутри класса персонажа Captain
<source lang="cpp">
virtual void visit(ObjectVisitor& Visitor) override {
    Visitor.visitCaptain(this);
};
</source>
или класса персонажа Whale
<source lang="cpp">
virtual void visit(ObjectVisitor& Visitor) override {
    Visitor.visitWhale(this);
};
</source>

<h3>Где скачать</h3>
<ul>
<li>win32-сборка доступна на сайте <a href="https://akk0rd87.itch.io/pirate-bomb">itch.io</a>
<li><a href="https://play.google.com/store/apps/details?id=org.akk0rdsdk.PirateBomb">Google Play</a>
<li><a href="https://apps.apple.com/ru/app/id1482437026">App store</a>
</ul>

<h3>Полезная литература и статьи</h3>
<ul>
<li><a href="https://www.ozon.ru/context/detail/id/211432335/?asb=kpvTmjl683OJaktf%252B5X2WR0n4yI4or3GQh%252BWOougzsI%253D">Паттерны объектно-ориентированного проектирования. Гамма Эрих, Хелм Ричард</a>
<li><a href="https://www.ozon.ru/context/detail/id/211433166/?asb=GhZHEi8FOzZG3N%252FvHY3xFEgT%252B5JeKMtdhFH7c%252F2UKh8%253D">Чистая архитектура. Искусство разработки программного обеспечения. Мартин Роберт</a>
<li><a href="https://habr.com/ru/post/136878/">Игровые циклы или ЭлектроКардиоГама</a>
</ul>