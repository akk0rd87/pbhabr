
Ранее я уже писал [https://habr.com/ru/post/433172/] о своем опыте разработки мобильных приложений на SDL.

Редактор уровней
Для создания и редактирования уровней использовал Tiled map editor [https://www.mapeditor.org]. Уровни (и более привычный термин для этого инструмента - карты), сохранял в дефолтном TMX-формате, что по своей сути представляет из себя обыкновенный XML-файл. Для самоконтороля набросал XSD-схему, и момент архивации (об этом далее) валидировал каждый уровень.

Архивация asset-ов
Так дефолтный формат достаточно громоздкий (хотя и очень наглядный), для сборки приложения будет использовать свой сжатый бинарный формат - будет назвать его далее как "архив". Перед архивацией все TMX-файлы проходят валидацию по вышеупомянутой XSD-схеме, так же некоторые другие логические проверки, которые нельзя вынести на XSD-уровень, такие как , например, проверка достижимостьивсех платформ с любой другой платформы и т.п.

Box2d